let user_input = document.querySelector('#country_select');
let form = document.querySelector('form');
let country = document.querySelector('.country');
let city = document.querySelector('.city');
let latitude = document.querySelector('.latitude');
let longitude = document.querySelector('.longitude');
let weather_main = document.querySelector('.weather_main');
let weather_description = document.querySelector('.weather_description');
let temp = document.querySelector('.temp');
let feels_like = document.querySelector('.feels_like');
let humidity = document.querySelector('.humidity');
let icon = document.querySelector('.icon_weather');

user_input.addEventListener('change',function(event){
    data = user_input.value;
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${data}&units=metric&appid=e95cf470715b92e1616e7cab5055d0d6`)
        .then(response => response.json())
        .then(data => {
            console.log(data)
            //country.textContent = data.sys.country;
            city.textContent = data.name;
            latitude.textContent = "latitude:"+data.coord.lat;
            longitude.textContent = "longitude:"+data.coord.lon;
            weather_main.textContent = "main:"+data.weather[0].main;
            weather_description.textContent = "description:"+data.weather[0].description;
            temp.textContent = data.main.temp+"°";
            feels_like.textContent = "feels_like:"+data.main.feels_like;
            humidity.textContent = "humidity:"+data.main.humidity
            icon.src = "http://openweathermap.org/img/wn/"+data.weather[0].icon+"@2x.png"
        })
});


